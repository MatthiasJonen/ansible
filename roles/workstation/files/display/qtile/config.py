# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# IMPORTS
from typing import List  # noqa: F401

from libqtile import qtile, bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Screen, Match
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
terminal = guess_terminal()
firefox = "flatpak run org.mozilla.firefox"
vscodium = "flatpak run com.vscodium.codium"
thunderbird = "flatpak run org.mozilla.Thunderbird"
element = "flatpak run im.riot.Riot"
signal = "flatpak run org.signal.Signal"
nextcloud = "flatpak run com.nextcloud.desktopclient.nextcloud"

keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down(),
        desc="Move focus down in stack pane"),
    Key([mod], "j", lazy.layout.up(),
        desc="Move focus up in stack pane"),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down(),
        desc="Move window down in current stack "),
    Key([mod, "control"], "j", lazy.layout.shuffle_up(),
        desc="Move window up in current stack "),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack"),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate(),
        desc="Swap panes of split stack"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown qtile"),
    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),

    # Launch applications
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "f", lazy.spawn(firefox), desc="Launch firefox in specified group"),
    Key([mod], "c", lazy.spawn(vscodium), desc="Launch vscodium in specified group"),
    Key([mod], "p", lazy.spawn("pcmanfm"), desc="Launch pcmanfm"),
    Key([mod], "n", lazy.spawn(nextcloud), desc="Launch nextcloud in specified group"),
    Key([mod], "m", lazy.spawn(thunderbird), desc="Launch thunderbird in specified group"),
    Key([mod], "e", lazy.spawn(element), desc="Launch element in specified group"),
    Key([mod], "s", lazy.spawn(signal), desc="Launch signal in specified group"),
]

# GROUPS
# groups for special applications
groups = []
groups.extend([
        Group("1", label="", spawn=[terminal], layout="monadtall", persist=True),
        Group("2", label="", spawn=[firefox], layout="max", persist=True,
        matches=[Match(wm_class=["Firefox"])]),
        Group("3", label="", spawn=[vscodium], layout="max", persist=True,
        matches=[Match(wm_class=["VSCodium"])]),
        Group("4", label="", spawn=["Pcmanfm", nextcloud], layout="monadtall", persist=True,
        matches=[Match(wm_class=["Nextcloud"])]),
        Group("5", label="", spawn=[thunderbird], layout="max", persist=True,
        matches=[Match(wm_class=["Thunderbird"])]),

        ])
# groups for random stuff
group_names = ["6", "7", "8", "9",]
group_labels = ["6", "7", "8", "9",]
group_layouts = ["monadtall", "max", "max", "monadtall",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

groups.extend([
    Group("0", label="", spawn=[signal, element], layout="monadtall", persist=True,
    matches=[Match(wm_class=["Element", "Signal"])]),
])

# bind keys to groups
for i in groups:
    keys.extend([
        # mod + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # mod + control + letter of group = move focused window to group
        Key([mod, "control"], i.name, lazy.window.togroup(i.name),
            desc="move focused window to group {}".format(i.name)),
    ])

# LAYOUTS
layout_theme = {"border_width": 2,
                "margin": 6,
                "border_focus": "e1acff",
                "border_normal": "1D2330"
                }


layouts = [
    layout.Max(**layout_theme),
    layout.Stack(num_stacks=2),
    # Try more layouts by unleashing below layouts.
    # layout.Bsp(),
    # layout.Columns(),
    # layout.Matrix(),
    layout.MonadTall(**layout_theme),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

colors = [
    ["#282c34", "#282c34"], # 0 panel background
    ["#434758", "#434758"], # 1 background for current screen tab
    ["#ffffff", "#ffffff"], # 2 font color for group names
    ["#ff5555", "#ff5555"], # 3 border line color for current tab
    ["#8d62a9", "#8d62a9"], # 4 border line color for other tab and odd widgets
    ["#668bd7", "#668bd7"], # 5 active window / main widget color
    ["#e1acff", "#e1acff"]  # 6 window name
]


widget_defaults = dict(
    font='RobotoMono Nerd Font',
    fontsize=12,
    padding=2,
    foreground = colors[2],
    background = colors[0],
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                    linewidth = 0,
                    padding = 6,
                    foreground = colors[2],
                    background = colors[0]
                ),
                widget.GroupBox(
                    fontsize = 16,
                    padding_y = 5,
                    padding_x = 3,
                    margin_y = 3,
                    margin_x = 0,
                    borderwidth = 3,
                    active = colors[5],
                    inactive = colors[2],
                    rounded = False,
                    highlight_color = colors[1],
                    highlight_method = "line",
                    this_current_screen_border = colors[3],
                    this_screen_border = colors [4],
                    other_current_screen_border = colors[0],
                    other_screen_border = colors[0],
                    foreground = colors[2],
                    background = colors[0],
                ),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Notify(
                    default_timeout = 60,
                    max_chars = 100,
                ),
                widget.CurrentLayout(),
                widget.TextBox(
                    text = " Vol:",
                    padding = 0
                ),
                widget.Volume(
                    padding = 5,
                    volume_app = "pavucontrol",
                ),
                widget.Systray(),
                widget.Clock(format='%y-%m-%d %H:%M'),
                widget.TextBox(
                    "",
                    fontsize = 16,
                    padding = 7,
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal+ " -e sudo shutdown now"),
                                        'Button3': lambda: qtile.cmd_spawn(terminal+ " -e sudo reboot")},
                ),
                widget.Sep(
                    linewidth = 0,
                    padding = 6,
                    foreground = colors[2],
                    background = colors[0]
                ),
            ],
            24,
            opacity = 0.5,
        ),
    ),
    Screen(),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
            start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
            start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
])
auto_fullscreen = True
focus_on_window_activation = "smart"


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
