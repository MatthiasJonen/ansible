#!/bin/bash

read -p "Is this a (p)rod or (s)taging instance (p/s)? " CHOICE
case "$CHOICE" in
  p|P ) INVENTORY=production;;
  s|S ) INVENTORY=staging;;
  * ) echo "invalid";;
esac

read -p "Please specify branch name for initial ansible-pull, leave blank for master:" BRANCH
[  -v $BRANCH ] && BRANCH=master


# create .ansiblevault password file with registry password
echo "Provide ansiblevault - secret: "
IFS= read -s  -p Password: SECRET

echo "$SECRET" | install -m 600 /dev/stdin /root/.ansiblevault

# install ansible
[ -f /etc/apt/sources.list.d/pve-enterprise.list ] && \
  rm /etc/apt/sources.list.d/pve-enterprise.list           # Add the no-subscription repo with playbook
[ -f /usr/bin/apt ] && \
  apt install -y git python3-setuptools python3-pip sudo ansible
[ -f /usr/sbin/pacman ] && \
  pacman -S git python python-pip sudo ansible --noconfirm

# run playbook
ansible-pull -U https://gitlab.com/MatthiasJonen/ansible.git --vault-password-file /root/.ansiblevault -i inventory/$INVENTORY --checkout "$BRANCH"