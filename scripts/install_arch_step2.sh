#!/usr/bin/bash

read -p "Provide disk name for installation (like sda): " disk

mkfs.fat -F32 /dev/"${disk}1"
mkswap /dev/"${disk}2"
swapon /dev/"${disk}2"
