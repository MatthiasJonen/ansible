#!/usr/bin/bash

# Main source: https://youtu.be/Xynotc9BKe8

# Do this in root@archiso
loadkeys de-latin1
timedatectl set-ntp true
reflector -c Austria -c Czech -c Slovakia -c Hungary -a 6 --sort rate --save /etc/pacman.d/mirrorlist
pacman -Syy

lsblk