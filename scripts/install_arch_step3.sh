#!/usr/bin/bash
read -p "Provide disk name for installation (like sda): " disk

mkfs.btrfs /dev/mapper/cryptroot

mount /dev/mapper/cryptroot /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@snapshots
btrfs subvolume create /mnt/@var_log

umount /mnt
mount -o noatime,compress=lzo,space_cache=v2,subvol=@ /dev/mapper/cryptroot /mnt
mkdir -p /mnt/{boot,home,.snapshots}
mkdir -p /mnt/var/log
mount -o noatime,compress=lzo,space_cache=v2,subvol=@home /dev/mapper/cryptroot /mnt/home
mount -o noatime,compress=lzo,space_cache=v2,subvol=@snapshots /dev/mapper/cryptroot /mnt/.snapshots
mount -o noatime,compress=lzo,space_cache=v2,subvol=@var_log /dev/mapper/cryptroot /mnt/var/log
mount /dev/"${disk}1" /mnt/boot

# Prepare statements for next steps in iso:
alias 1_pacstrap="pacstrap /mnt base linux linux-firmware neovim"
alias 2_genfstab="genfstab -U /mnt >> /mnt/etc/fstab"

# Prepare statements for next steps in arch-chroot
mkdir -p /mnt/root
touch /mnt/root/.zshrc
echo 'alias 1_timezone="ln -sf /usr/share/zoneinfo/Europe/Vienna /etc/localtime"' >> /mnt/root/.zshrc
echo 'alias 2_hwclock="hwclock --systohc"' >> /mnt/root/.zshrc
echo 'alias 3_install_base="pacman -S grub efibootmgr networkmanager network-manager-applet snapper"' >> /mnt/root/.zshrc
echo 'alias 4_grub="grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB"' >> /mnt/root/.zshrc
echo 'alias 5_grub="grub-mkconfig -o /boot/grub/grub.cfg"' >> /mnt/root/.zshrc
echo 'alias 6_enable="systemctl enable NetworkManager"' >> /mnt/root/.zshrc