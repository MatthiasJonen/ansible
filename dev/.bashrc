# .bashrc for root in docker container. Helps me to test easily.
# Path variable
export PATH=$PATH:$HOME/.local/bin

#Some more alias to avoid making mistakes:
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

#Some more alias to test my ansible provision
alias defhost='echo -e "[server]\n\n[workstation]\n" >> /root/hosts && sed -i "/server/a$HOSTNAME ansible_connection=local" /root/hosts'
alias defworkstation='echo -e "[server]\n\n[workstation]\n" >> /root/hosts && sed -i "/workstation/a$HOSTNAME ansible_connection=local" /root/hosts'
alias play1_local='ansible-playbook --limit $HOSTNAME --vault-password-file /root/.ansiblevault --extra-vars ansible_python_interpreter=/usr/bin/python3 /opt/local.yml -i /root/hosts'

#Helpful aliases for debugging and information gathering
alias ansible_facts='ansible localhost -m setup > /opt/dev/ansible_facts'

#Helpful aliases for xsession docker containers
alias keyboard_layout='setxkbmap at'
alias backup_config='cp -r .config backup_config && cp -r .local backup_local && cp -r .kde backup_kde'
alias diff_backup='diff -qr .config backup_config && diff -qr .local backup_local && diff -qr .kde backup_kde | tee /opt/dev/diff_backup_output'