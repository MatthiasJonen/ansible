#!/usr/bin/python3
import sys
from pathlib import Path


def main():
    """Find Vault files and check encryption.

    If a vault file is not encrypted, exit pre-commit.
    """
    for file in find_file(find_toplevel_dir(),"vault"):
        if check_encyrption(file,"$ANSIBLE_VAULT;1.1;AES256") == False:
            print(str(file) + " Is not encrypted, but it should be before publishing it. Use ./dev/encrypt.sh to solve the issue.")
            sys.exit(1)


def find_toplevel_dir(directory: str = None) -> str:
    """
    Toplevel directory contains .git folder. Find it

    Find the correct git dir. Move upwards if .git folder is not found here.
    """
    if directory == None:
        directory = Path.cwd()
    elif Path.is_dir(Path(directory)):
        directory = Path(directory)
    else:
        print("Given string is no directory.")
        sys.exit(1)

    gitdir = directory.joinpath(".git")
    while Path.is_dir(gitdir) == False:
        directory = directory.parent
        gitdir = directory.joinpath(".git")
    else:
        return directory


def find_file(directory: str, file: str) -> list:
    "Find all desired files recursively from directory"
    path = Path(directory)
    pattern = "**/*" + file
    return list(path.glob(pattern))


def check_encyrption(file, encrytion_itentifier: str) -> bool:
    """Check first line of file and against encryption itentifier.

    Based on assumption that you can itentify encryption on the first line of the file.
    Works with ansible-vault
    Returns true if encrypted, otherwise false
    """
    with open(file) as file:
        first_line = file.readline()
        if first_line == encrytion_itentifier + "\n":
            return True
        else:
            return False


if __name__ == "__main__":
	main()
