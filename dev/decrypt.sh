#!/bin/bash

toplevel=$(git rev-parse --show-toplevel)

# decrypt all vault files if they are encrypted
find $toplevel -name "*vault*" -not -path "*/.git/*" | xargs -L1 sudo ansible-vault decrypt --vault-password-file /root/.ansiblevault
find $toplevel -name "*vault*" -not -path "*/.git/*" | xargs -L1 sudo chown matthias:matthias
find $toplevel -name "*vault*" -not -path "*/.git/*" | xargs -L1 sudo chmod 664
find $toplevel -name "*vault*" -not -path "*/.git/*" | xargs -L1 echo