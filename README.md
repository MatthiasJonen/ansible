# ansible

My ansible configuration for homelab server and workstations. Inspired by learnlinux.tv

CICD Using gitlab runner and docker first and proxmox vms in staging. This is inspired by [kruyt.org](https://kruyt.org/ansible-ci-with-gitlab/)

## Users Manual
### KDE specific
I use konsave for plasma configuration persistence. The profiles are provided by this ansible repo in ~/.config/konsave.
For the relevant konsave I like the ``` konsave -h ```
## Baseinstall for arch
Main source for this is [EF- Tech Made Simple](https://youtu.be/Xynotc9BKe8)
### Base setup

```
loadkeys de-latin1
pacman -Syy
pacman -S wget
wget https://gitlab.com/MatthiasJonen/ansible/-/raw/master/scripts/install_arch_step1.sh
wget https://gitlab.com/MatthiasJonen/ansible/-/raw/master/scripts/install_arch_step2.sh
chmod +x install_arch*.sh
./install_arch_step1.sh
```
### Manually partition the disk
Manually partition the disk. In my case it is sda
`gdisk /dev/sda`

n for new partition (efi)

defaults until Last sector: +300M

Hex Code ef00

n for new partition (swap)

defaults until Last sector: +2G

Hex Code 8200

n for new partition

only defaults

w

### Formating, mounting and install base packages
`./install_arch_step2.sh`
When prompted type your disk name (like sda)

I want encryption: Example is with diskname sda

`cryptsetup -y -v luksFormat /dev/sda3`

Accept the prompt

`cryptsetup open /dev/sda3 cryptroot`

`./install_arch_step3.sh`

after the script:
```
pacstrap /mnt base linux linux-firmware neovim

genfstab -U /mnt >> /mnt/etc/fstab
```
or use the aliases for this...

### Chroot /mnt

`arch-chroot /mnt`
#### Define hostname and hosts
```
nvim /etc/hostname
nvim /etc/hosts #TODO: ansible playbook
127.0.0.1   localhost
::1         localhost
127.0.1.1   <hostname>
source /root/.zshrc
```
Use .zshrc aliases 1_timezone and 2_hwclock

#### Change root password
`passwd`

Use alias 3_install_base
```
nvim /etc/mkinitcpio.conf
# put btfrs in MODULES
MODULES=(btrfs)
# put encryption in hooks
HOOKS=(base udev autodetect keyboard keymap modconf block encrypt filesystems fsck)
# save and exit the file
mkinitcpio -p linux

# Use aliases 4 till 6.

# find out UUID of enctrypted volume
blkid > uuid
nvim uuid
# copy the line with the encrypted volume
nvim /etc/default/grub
# Go to end of line of GRUB_CMDLINE_LINUX="" Paste copied text
# Edit the GRUB_CMDLINE_LINUX="" to this endresult:
GRUB_CMDLINE_LINUX="cryptdevice=UUID=<UUID>:cryptroot root=/dev/mapper/cryptroot"
# save and exit the file
# Use alias 5 once again

exit
umount -a
reboot
```


## Development Info
### Install git hooks
* To install git hooks execute the `./dev/install_hooks.sh`
* Make sure all confident information is in files named '*vault'. E.g 'vault', 'secret_vault'
* The following scripts help with encryption and decryption.
  ```
  ./dev/encrypt.sh
  ./dev/decrypt.sh
  ```

### Set up new host
Important: Add host in respective inventory file. Otherwise the ansible pull command won't do anything.

For convenience install ssh and start it one without enabling it.
Enabling is no good idea with PermitRootLogin yes
```
loadkeys de-latin1
pacman -S wget openssh
nvim /etc/ssh/sshd_config
PermitRootLogin yes
systemctl start sshd
```

Become root, download the relevant bash script and run it.
```
wget https://gitlab.com/MatthiasJonen/ansible/-/raw/master/scripts/install_ansible_pull.sh
chmod +x install_ansible_pull.sh
./install_ansible_pull.sh
```

### Create local docker images for local testing
Run the docker build command and use a Dockerfile in directory dockerfiles for it. eg.
```
docker build -t 'debian_server' -f 'dockerfiles/Dockerfile_debian_buster_server' .
```

### Run docker container for local testing
The last argument is the name of the image specified in the docker build command
```
docker run -it --mount src="$(pwd)",target=/opt,type=bind -v /root/.ansiblevault:/root/.ansiblevault:ro debian_server
# Special case KDEneon image. It needs Xephyr you can test in an X application
Xephyr -screen 1024x768 :1 & docker run -it --mount src="$(pwd)",target=/opt,type=bind -v /root/.ansiblevault:/root/.ansiblevault:ro -v /tmp/.X11-unix:/tmp/.X11-unix kdeneon
```
### Source .bashrc in container for local testing
This is helpful to have proper aliases for testing commands
```
source /opt/dev/.bashrc
```
